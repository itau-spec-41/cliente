package br.com.itau.Customer.repository;

import br.com.itau.Customer.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
